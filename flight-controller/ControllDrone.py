#! /usr/bin/env python
import time, sys
import ps_drone as ps_drone #Imports the PS-Drone-API

def main():
	drone = ps_drone.Drone() #Initials the PS-Drone-API
	drone = prepare_drone(drone)
	drone = enable_video(drone)
	drone = control_drone(drone)


def control_drone(drone):
	print "Control drone enabled..."
	print "		AWSD, Y - up, H - down"
	print "		Q - turnLeft, E - turnRght"
	print "		I - stop, P - land&Exit"
	key = None
	print "Click <space> to take off:"
	key = drone.getKey()
	if key == " ": drone.takeoff()
	while True:
		key = drone.getKey()
		if key == "w":
			drone.moveForward()
		elif key == "s":
			drone.moveBackward()
		elif key == "a":
			drone.moveLeft()
		elif key == "d":
			drone.moveRight()
		elif key == "y":
			drone.moveUp()
		elif key == "h":
			drone.moveDown()
		elif key == "q":
			drone.turnLeft()
		elif key == "e":
			drone.turnRight()
		elif key == "i":
			drone.stop()
		elif key == "p":
			print("Landing & Exiting ...")
			drone.land()
			time.sleep(3)
			sys.exit()

		print "Pressed: ["+key+"] Something is: " + str(drone.NavData["demo"][0][2])


def enable_video(drone):
	print "Enabling video..."
	drone.setConfigAllID()
	drone.sdVideo() #Choose lower resolution
	drone.frontCam() #Choose front view
	while CDC==drone.ConfigDataCount: time.sleep(0.001) #Wait until it is done
	drone.startVideo() #Start video-function
	drone.showVideo() #Display the video
	print "Front view enabled..."

	IMC = drone.VideoImageCount #Number of encoded videoframes
	#stop = False
	#switchCamera = False #To toggle front- and groundcamera
	#while drone.VideoImageCount==IMC: time.sleep(0.01) #Wait for next image
	#	IMC = drone.VideoImageCount #Number of encoded videoframes
	#	key = drone.getKey()
	#	if key== " ":
	#		if switchCamera: 
	#			switchCamera = False
	#		else: 
	#			switchCamera = True
	#		drone.groundVideo(switchCamera) #Toggle between front- and groundcamera.
	#	elif key and key !=  " ": stop = True
	return drone



##### Suggested clean drone startup sequence #####
def prepare_drone(drone, speed=0.1):
	print "Connecting to Drone & Reseting..."
	drone.startup() #Connects to the drone and starts subprocesses
	drone.reset() #Sets drones LEDs to green when red
	while (drone.getBattery()[0]==-1): time.sleep(0.1) #Reset completed ?
	print "Battery: "+str(drone.getBattery()[0])+ "% "+str(drone.getBattery()[1])
	if drone.getBattery()[1]== "empty": sys.exit()
	drone.useDemoMode(True) #15 basic datasets per second (default)
	drone.getNDpackage(["demo"]) #Packets, which shall be decoded
	time.sleep(0.5) #Give it some time to fully awake
	drone.trim() #Recalibrate sensors
	print "Calibrating gyroscope..."
	drone.getSelfRotation(5) #Auto-alteration-value of gyroscope-sensor
	print  "Auto-alternation:  "+ str(drone.selfRotation)+"deg/sec"
	drone.setSpeed(speed)
	print "Set drone's speed to: " + str(speed) + "%\nReady to take-off..."
	return drone

if __name__ == '__main__':
    main()