#! /usr/bin/env python
import time
import ps_drone as ps_drone #Imports the PS-Drone-API
drone = ps_drone.Drone() #Initials the PS-Drone-API
drone.startup() #Connects to the drone and starts subprocesses
drone.takeoff() #Drone starts
time.sleep(4) #Gives the drone time to start
#drone.setSpeed(0.1) #Sets default moving speed to 1.0 (=100%)
#drone.moveForward(0.2) #Drone flies forward...
#time.sleep(2) #... for two seconds
#drone.stop() #Drone stops...
#time.sleep(2) #... needs, like a car, time to stop
#drone.moveBackward() #Drone flies backward with a quarter speed...
#time.sleep(1.5) #... for one and a half seconds
#drone.stop() #Drone stops
#time.sleep(2)
drone.turnLeft() #Drone moves full speed to the left...
time.sleep(3) #... for two seconds
#drone.stop() #Drone stops
#time.sleep(2)
drone.land() #Drone lands